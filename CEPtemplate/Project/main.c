/*
 * Code belongs to TI4 CE; Informatik; HAW-Hamburg; Berliner Tor 7; D-20099 Hamburg
 * Code was based on demo examples from Silke Behn, Heiner Heitmann & Bernd Schwarz
 * Code was "mixed up" by Michael Sch�fers and was originally designed for Digilent Nexys2 Board
 * Code was adapted/ported to Silica Xynergy Board by Yannic Wilkening
 * Code was "pimped up" by Michael Sch�fers (e.g. adding comments and references to documentation)
*/

/*
 * Header-Dateien, die benoetigt werden
 */
#include <stdint.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include "CE_Lib.h"
#include "tft.h"

/*
 * This macro is used to type values in binary
 */
#define b(n) (                                     \
    (unsigned char)(                               \
          ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
      |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
      |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                          \
)

/*
 * simplified access to switches S0 - S7
 */
#define  S8   ( !(GPIOH->IDR & (b(1) << 15)) )
#define  S7   ( !(GPIOH->IDR & (b(1) << 12)) )
#define  S6   ( !(GPIOH->IDR & (b(1) << 10)) )
#define  S5   ( !(GPIOF->IDR & (b(1) << 8 )) )
#define  S4   ( !(GPIOF->IDR & (b(1) << 7 )) )
#define  S3   ( !(GPIOF->IDR & (b(1) << 6 )) )
#define  S2   ( !(GPIOC->IDR & (b(1) << 2 )) )
#define  S1   ( !(GPIOI->IDR & (b(1) << 9 )) )



#define  STM32_NVIC_PriorityGroup_0   ((uint32_t)0x7)         /*!< 0 bits for pre-emption priority 4 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_1   ((uint32_t)0x6)         /*!< 1 bits for pre-emption priority 3 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_2   ((uint32_t)0x5)         /*!< 2 bits for pre-emption priority 2 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_3   ((uint32_t)0x4)         /*!< 3 bits for pre-emption priority 1 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_4   ((uint32_t)0x3)         /*!< 4 bits for pre-emption priority 0 bits for subpriority */



/*
 * timer setup
 * ============
 */
#define SYS_FREQ     168000000                             // 168MHz system frequency
#define TIMER_FREQ       44100                             // 44.1Khz requested timer frequency
#define MANUAL_FREQ1       440                             //440hz
#define MANUAL_FREQ2       5000                            //5Khz
#define AMPLITUDE_1V  0                                    //amplitude 1V flag
#define AMPLITUDE_05V 1                                    //amplitude 0.5V flag
#define MEDIAN 1800                                        //median value for 1.5V


// buffer_BUFF_SIZE has to equal 2^n                             (for buffer_INDX_MSK)
#define   buffer_BUFF_SIZE   ( b(1) << 2 )                      /*!< 4 entries buffer depth resp. buffer size */
#define   buffer_EMPTY 0
#define   buffer_INDX_MSK   ( buffer_BUFF_SIZE - 1 )
#define   Q9_22SHIFT 22
#define   Q16_47SHIFT 47                                   //highest resolution because of TIMER_FREQ is highest value in calculation
#define   BACK_TO_Q9_22 25								   // 47-22=25

#define   SIG_TAB_NOE 360

#define   LIMIT_PRECISED (SIG_TAB_NOE<<Q9_22SHIFT)

#define   INCREMENT_FOR_440  (uint32_t)(((360ULL<<Q16_47SHIFT)/((44100ULL<<Q16_47SHIFT)/(440ULL<<Q16_47SHIFT)))>>BACK_TO_Q9_22)
#define   INCREMENT_FOR_5000 (uint32_t)(((360ULL<<Q16_47SHIFT)/((44100ULL<<Q16_47SHIFT)/(5000ULL<<Q16_47SHIFT)))>>BACK_TO_Q9_22)

#define USE_TFT
#define VERSION "Version 1.0"

static volatile int buffer[ buffer_BUFF_SIZE ] = { 0 };    // buffer BUFFER
static volatile uint16_t bufferElements = 0;               // bufferElements
static volatile uint16_t bufferRdIndx = 0;                 // buffer Read Index
static volatile int8_t   bufferUnderflow = 0;              // "buffer UNDERFLOW was detected" state

//SINUS TABLE
static const int32_t sineTable[SIG_TAB_NOE] = 
{
    0,        22,       43,       65,       87,       108,      130,      152,      173,       195,
    216,      237,      259,      280,      301,      322,      343,      364,      384,       405,
    426,      446,      466,      486,      506,      526,      545,      565,      584,       603,
    622,      641,      659,      678,      696,      713,      731,      749,      766,       783,
    799,      816,      832,      848,      864,      879,      895,      909,      924,       938,
    953,      966,      980,      993,      1006,     1018,     1031,     1043,     1054,      1065,
    1076,     1087,     1097,     1107,     1117,     1126,     1135,     1144,     1152,      1160,
    1168,     1175,     1182,     1188,     1194,     1200,     1205,     1210,     1215,      1219,
    1223,     1226,     1230,     1232,     1235,     1237,     1238,     1239,     1240,      1241,
    1241,     1241,     1240,     1239,     1237,     1236,     1233,     1231,     1228,      1225,
    1221,     1217,     1212,     1208,     1203,     1197,     1191,     1185,     1178,      1171,
    1164,     1156,     1148,     1140,     1131,     1122,     1112,     1102,     1092,      1082,
    1071,     1060,     1048,     1037,     1025,     1012,     999,      986,      973,       959,
    946,      931,      917,      902,      887,      872,      856,      840,      824,       808,
    791,      774,      757,      740,      722,      705,      687,      668,      650,       631,
    613,      594,      574,      555,      536,      516,      496,      476,      456,       436,
    415,      395,      374,      353,      333,      312,      290,      269,      248,       227,
    205,      184,      162,      141,      119,      98,       76,       54,       33,        11,
    -11,      -33,      -54,      -76,      -98,      -119,     -141,     -162,     -184,      -205,
    -227,     -248,     -269,     -290,     -312,     -333,     -353,     -374,     -395,      -415,
    -436,     -456,     -476,     -496,     -516,     -536,     -555,     -574,     -594,      -613,
    -631,     -650,     -668,     -687,     -705,     -722,     -740,     -757,     -774,      -791,
    -808,     -824,     -840,     -856,     -872,     -887,     -902,     -917,     -931,      -946,
    -959,     -973,     -986,     -999,     -1012,    -1025,    -1037,    -1048,    -1060,     -1071,
    -1082,    -1092,    -1102,    -1112,    -1122,    -1131,    -1140,    -1148,    -1156,     -1164,
    -1171,    -1178,    -1185,    -1191,    -1197,    -1203,    -1208,    -1212,    -1217,     -1221,
    -1225,    -1228,    -1231,    -1233,    -1236,    -1237,    -1239,    -1240,    -1241,     -1241,
    -1241,    -1240,    -1239,    -1238,    -1237,    -1235,    -1232,    -1230,    -1226,     -1223,
    -1219,    -1215,    -1210,    -1205,    -1200,    -1194,    -1188,    -1182,    -1175,     -1168,
    -1160,    -1152,    -1144,    -1135,    -1126,    -1117,    -1107,    -1097,    -1087,     -1076,
    -1065,    -1054,    -1043,    -1031,    -1018,    -1006,    -993,     -980,     -966,      -953,
    -938,     -924,     -909,     -895,     -879,     -864,     -848,     -832,     -816,      -799,
    -783,     -766,     -749,     -731,     -713,     -696,     -678,     -659,     -641,      -622,
    -603,     -584,     -565,     -545,     -526,     -506,     -486,     -466,     -446,      -426,
    -405,     -384,     -364,     -343,     -322,     -301,     -280,     -259,     -237,      -216,
    -195,     -173,     -152,     -130,     -108,     -87,      -65,      -43,      -22,       0,
};

//SAWTOOTH TABLE
static const int32_t sawtoothTable[SIG_TAB_NOE] = 
{
    -1241,  -1234,  -1227,  -1220,  -1213,  -1206,  -1199,  -1193,  -1186,  -1179,
    -1172,  -1165,  -1158,  -1151,  -1144,  -1137,  -1130,  -1123,  -1116,  -1110,
    -1103,  -1096,  -1089,  -1082,  -1075,  -1068,  -1061,  -1054,  -1047,  -1040,
    -1034,  -1027,  -1020,  -1013,  -1006,  -999,   -992,   -985,   -978,   -971,
    -964,   -957,   -951,   -944,   -937,   -930,   -923,   -916,   -909,   -902,
    -895,   -888,   -881,   -875,   -868,   -861,   -854,   -847,   -840,   -833,
    -826,   -819,   -812,   -805,   -798,   -792,   -785,   -778,   -771,   -764,
    -757,   -750,   -743,   -736,   -729,   -722,   -716,   -709,   -702,   -695,
    -688,   -681,   -674,   -667,   -660,   -653,   -646,   -639,   -633,   -626,
    -619,   -612,   -605,   -598,   -591,   -584,   -577,   -570,   -563,   -557,
    -550,   -543,   -536,   -529,   -522,   -515,   -508,   -501,   -494,   -487,
    -480,   -474,   -467,   -460,   -453,   -446,   -439,   -432,   -425,   -418,
    -411,   -404,   -398,   -391,   -384,   -377,   -370,   -363,   -356,   -349,
    -342,   -335,   -328,   -321,   -315,   -308,   -301,   -294,   -287,   -280,
    -273,   -266,   -259,   -252,   -245,   -239,   -232,   -225,   -218,   -211,
    -204,   -197,   -190,   -183,   -176,   -169,   -162,   -156,   -149,   -142,
    -135,   -128,   -121,   -114,   -107,   -100,   -93,    -86,    -80,    -73,
    -66,    -59,    -52,    -45,    -38,    -31,    -24,    -17,    -10,    -3,
    3,      10,     17,     24,     31,     38,     45,     52,     59,     66,
    73,     80,     86,     93,     100,    107,    114,    121,    128,    135,
    142,    149,    156,    162,    169,    176,    183,    190,    197,    204,
    211,    218,    225,    232,    239,    245,    252,    259,    266,    273,
    280,    287,    294,    301,    308,    315,    321,    328,    335,    342,
    349,    356,    363,    370,    377,    384,    391,    398,    404,    411,
    418,    425,    432,    439,    446,    453,    460,    467,    474,    480,
    487,    494,    501,    508,    515,    522,    529,    536,    543,    550,
    557,    563,    570,    577,    584,    591,    598,    605,    612,    619,
    626,    633,    639,    646,    653,    660,    667,    674,    681,    688,
    695,    702,    709,    716,    722,    729,    736,    743,    750,    757,
    764,    771,    778,    785,    792,    798,    805,    812,    819,    826,
    833,    840,    847,    854,    861,    868,    875,    881,    888,    895,
    902,    909,    916,    923,    930,    937,    944,    951,    957,    964,
    971,    978,    985,    992,    999,    1006,   1013,   1020,   1027,   1034,
    1040,   1047,   1054,   1061,   1068,   1075,   1082,   1089,   1096,   1103,
    1110,   1116,   1123,   1130,   1137,   1144,   1151,   1158,   1165,   1172,
    1179,   1186,   1193,   1199,   1206,   1213,   1220,   1227,   1234,   1241,
};

//----------------------------------------------------------------------------
//
//  ISR
//
void TIM1_UP_TIM10_IRQHandler(void)
{                        
    TIM1->SR = ~TIM_SR_UIF;
    if (bufferElements > buffer_EMPTY )
    {    
        DAC->DHR12R1 = buffer[bufferRdIndx];
        bufferRdIndx = (bufferRdIndx + 1) & buffer_INDX_MSK;
        --bufferElements;     
    }
    else
    {
        bufferUnderflow = 1;
        TFT_puts("Underflow");
    }
}

//----------------------------------------------------------------------------
//
//  MAIN
//
int main( void )
{  
	uint8_t amplitude = AMPLITUDE_1V;
    uint32_t increment = INCREMENT_FOR_440;
    uint16_t bufferWrIndx = 0;
    uint32_t bufferNextValue = 0;   
    int32_t const* pTable = sineTable;
    /*
     * general setup
     * =============
     */
    initCEP_Board();                                    // initilize display, leds, buttons, uart and other stuff CE_Lib.c
    RCC->AHB1ENR |= (   RCC_AHB1ENR_GPIOAEN             // enable clock for GPIOA               {>RM0090 chap 6.3.10}
                  | RCC_AHB1ENR_GPIOCEN                 // enable clock for GPIOC               {>s.a.}
                  | RCC_AHB1ENR_GPIOFEN                 // enable clock for GPIOF               {>s.a.}
                  | RCC_AHB1ENR_GPIOHEN                 // enable clock for GPIOH               {>s.a.}
                  | RCC_AHB1ENR_GPIOIEN                 // enable clock for GPIOI               {>s.a.}
    );
    //
    RCC->APB1ENR |= RCC_APB1ENR_DACEN;                  // enable clock for DAC                 {>RM0090 chap 6.3.13}
    //
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;                 // enable clock for timer 8             {>RM0090 chap 6.3.14}
  
    TIM1->CR1 = 0;                                          // disabled timer                   {>RM0090 chap 17.4.1)  
    TIM1->CR2 = 0;                                          //                                  {>RM0090 chap 17.4.2}
    TIM1->PSC = 0;                                          // NO prescaler                     {>RM0090 chap 17.4.11} //can divide counter clock frequence between factors 0 and 65536
    TIM1->ARR = (SYS_FREQ / TIMER_FREQ) -1;                 // 48000Hz                          {>RM0090 chap 17.4.12} //counts up every clkPeriod, after overflow it gets an update event
    TIM1->DIER = TIM_DIER_UIE;                              // enable interrupt                 {>RM0090 chap 17.4.4}
    TIM1->CR1 = TIM_CR1_ARPE;                               // auto-reload preload enable       {>RM0090 chap 17.4.1}
    
    NVIC_SetPriorityGrouping( STM32_NVIC_PriorityGroup_2 );       // 2bit preEmptionPrio & 2bit subPrio
    NVIC_SetPriority( TIM1_UP_TIM10_IRQn,0);                      // preEmptionPrio:=1 & subPrio:=0
    NVIC_EnableIRQ( TIM1_UP_TIM10_IRQn );                         // enable IRQ
    
    
    GPIOA->MODER = (GPIOA->MODER & ~(b(11) << (GPIO_PinSource4 * 2))) | (GPIO_Mode_AN << (GPIO_PinSource4 * 2));
    DAC->CR = 0;                                   // ControlRegister: configure / keep defaults     {>RM0090 chap 14.5.1}
    DAC->CR |= DAC_CR_EN1;                         // ControlRegister: Enable DAC channel1           {>RM0090 chap 14.5.1}
    DAC->CR |= DAC_CR_WAVE1;
    
    #ifdef USE_TFT
    // LCD info
    // ========
    //
    TFT_cls();
    TFT_gotoxy(1,1);
    TFT_puts("[CE A3 S2T5]");
    TFT_gotoxy(1,2);
    TFT_puts(VERSION);
    TFT_gotoxy(1,3);
    TFT_puts("S8: 5000Hz");
    TFT_gotoxy(1,4);
    TFT_puts("S7: 440Hz");
    TFT_gotoxy(1,5);
    TFT_puts("S6: Sinus Signal");
    TFT_gotoxy(1,6);
    TFT_puts("S5: Saegezahn Signal");
    TFT_gotoxy(1,7);
    TFT_puts("S4: Amplitude: 1.0V, Mittelwert: 1.5V");
    TFT_gotoxy(1,8);
    TFT_puts("S3: Amplitude: 0.5V, Mittelwert: 1.5V");
    TFT_gotoxy(1,9);
    TFT_puts("S2: Ausgabe starten");
    TFT_gotoxy(1,10);
    TFT_puts("S1: Ausgabe stoppen");
    #endif
    
    do{
        buffer[bufferWrIndx] = MEDIAN+(pTable[bufferNextValue>>Q9_22SHIFT]>>amplitude);
        bufferNextValue += increment;
        bufferElements = bufferWrIndx+1;
        bufferWrIndx = (bufferWrIndx + 1) % buffer_BUFF_SIZE;
    } while (bufferElements != buffer_BUFF_SIZE);
    
    while(1)
    {
        if (bufferElements == buffer_EMPTY)
        {
            do
            {
                buffer[bufferWrIndx] = MEDIAN + (pTable[bufferNextValue>>Q9_22SHIFT]>>amplitude);
                bufferNextValue += increment;
                if (bufferNextValue > LIMIT_PRECISED)
                {
                    bufferNextValue = 0;
                }
                bufferElements++;
                bufferWrIndx = (bufferWrIndx + 1) % buffer_BUFF_SIZE;
            } while (bufferElements != buffer_BUFF_SIZE);       
       }

        if( S8 )
        {
            increment = INCREMENT_FOR_5000;
        }

        if( S7 )
        {
            increment = INCREMENT_FOR_440;
        }
      
        if ( S6 )
        {
            pTable = sineTable;
        }
       
        if ( S5 )
        {
            pTable = sawtoothTable;
        }
       
        if ( S4 )
        {
            amplitude = AMPLITUDE_1V;
        }
       
        if ( S3 )
        {
            amplitude = AMPLITUDE_05V;
        }
       
        if ( S2 )
        {
            GPIOI->ODR |= (b(1)<<4);
            TIM1->CR1 |= TIM_CR1_CEN;           
        }
       
        if ( S1 )
        {
            GPIOI->ODR &= ~(b(1)<<4);
            TIM1->CR1 &=~TIM_CR1_CEN;           
        }
    }
}//main()



